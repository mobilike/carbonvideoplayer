#
#  Be sure to run `pod spec lint CarbonVideoPlayer.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "CarbonVideoPlayer"
  s.version      = "0.1.1"
  s.summary      = "A short description of CarbonVideoPlayer. This description was described as unmeaningful but who decides and who cares are 2 questions remaining at this point in development."
  s.description  = "A longer description of CarbonVideoPlayer."
  s.homepage     = "http://operamediaworks.com/"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"
  s.license      = "MIT"
  s.author             = { "Tolga Caner" => "tolga@opera.com" }
  s.platform     = :ios
  s.ios.deployment_target = "7.1"
  s.source       = { :git => "https://bitbucket.org/mobilike/carbonvideoplayer", :tag => "{s.version}" }

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  s.source_files  = "CarbonVideoPlayer", "CarbonVideoPlayer/**/*.{h,m}"
  #s.exclude_files = "Classes/Exclude"
  # s.public_header_files = "Classes/**/*.h"

  s.resources = "CarbonVideoPlayer/*.{png,xib,storyboard,strings}"

  # s.preserve_paths = "FilesToSave", "MoreFilesToSave"
  
  s.frameworks = "AdSupport", "WebKit"
  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  s.dependency 'Google-Mobile-Ads-SDK'
  s.dependency 'GoogleAds-IMA-iOS-SDK-For-AdMob' , '3.2.1'
  #s.dependency 'GoogleMobileAds' , '7.7.0'

end
