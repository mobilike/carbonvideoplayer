//
//  NSBundle+CarbonVideoPlayerBundle.m
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 06/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import "NSBundle+CarbonVideoPlayerBundle.h"
#import "CBVideoPlayer.h"

@implementation NSBundle (CarbonVideoPlayerBundle)

+ (NSBundle *)CVPFrameworkBundle
{
    static NSBundle* frameworkBundle = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        NSString* mainBundlePath = [[NSBundle bundleForClass:[CBVideoPlayer class]] resourcePath];
        NSString* frameworkBundlePath = [mainBundlePath  stringByAppendingPathComponent:@"CarbonVideoPlayer.framework"];
        frameworkBundle = [NSBundle bundleWithPath:frameworkBundlePath];
        if(!frameworkBundle)
        {
            frameworkBundlePath = [[mainBundlePath stringByAppendingPathComponent:@"Frameworks"] stringByAppendingPathComponent:@"CarbonVideoPlayer.framework"];
            frameworkBundle = [NSBundle bundleWithPath:frameworkBundlePath];
        }
        if(!frameworkBundle)
        {
            frameworkBundle = [NSBundle mainBundle];
        }
    });
    
    return frameworkBundle;
}

@end
