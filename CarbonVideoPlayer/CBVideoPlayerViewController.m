//
//  CBVideoPlayerViewController.m
//  Carbon
//
//  Created by Tolga Caner on 13/04/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "CBVideoPlayerViewController.h"
#import "GMFIMASDKAdService.h"
#import "CBVideoPlayer.h"

@interface CBVideoPlayerViewController () {
    int lastPlayedSecond;
    int seekStartTime;
    int totalSeekDifference;
}

@property (nonatomic,strong) NSString* midRollTag;
@property (nonatomic,strong) NSString* postRollTag;
@property (nonatomic,strong) NSNumber* frequency;
@property (nonatomic,weak) CBVideoPlayer* parentVideoPlayerViewController;

@end

@implementation CBVideoPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) videoPlayer:(GMFVideoPlayer *)videoPlayer stateDidChangeFrom:(GMFPlayerState)fromState to:(GMFPlayerState)toState{
    
    //----- Seeking
    if (fromState == kGMFPlayerStateSeeking) {
        totalSeekDifference += (seekStartTime - videoPlayer.currentMediaTime);
    } else if (toState == kGMFPlayerStateSeeking) {
        seekStartTime = videoPlayer.currentMediaTime;
    }
    //----- Seeking
    
    //----- Error
    if (toState == kGMFPlayerStateError) {
        [self.videoPlayer clearPlayerItem];
        GMFIMASDKAdService* gmfAdService = (GMFIMASDKAdService*)self.adService;
        [gmfAdService reset];
        [self dismiss];
    }
    //----- Error
    
    //----- Finished, request postroll
    if (toState == kGMFPlayerStateFinished && !self.dismissUponPostrollFinish) {
        self.dismissUponPostrollFinish = YES;
        [self requestMidOrPostRollWithTag:self.postRollTag];
        [self.videoPlayer clearPlayerItem];
        [self.parentVideoPlayerViewController videoPlaybackPausedForPostroll];
    }  else {
        [super videoPlayer:videoPlayer stateDidChangeFrom:fromState to:toState];
    }
}

- (void) dismiss {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(),^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf dismissViewControllerAnimated:true completion:nil];
        }
    });
    
}

- (void) adClicked {
    if ([[self parentViewController] isKindOfClass:[CBVideoPlayer class]]) {
        CBVideoPlayer* cbVideoPlayer = (CBVideoPlayer*) [self parentViewController];
        [cbVideoPlayer adClicked];
    }
}

- (void) setMidRollTag:(NSString*)midRollTag midRollFrequency:(NSNumber*)frequency withPostrollTag:(NSString*)postRollTag {
    self.dismissUponPostrollFinish = NO;
    lastPlayedSecond = 0;
    totalSeekDifference = 0;
    self.midRollTag = midRollTag;
    self.postRollTag = postRollTag;
    if ([frequency isEqualToNumber:@0]) {
        frequency = [NSNumber numberWithInt:-1];
    }
    [self setFrequency:frequency];
}

- (void) videoPlayer:(GMFVideoPlayer *)videoPlayer currentMediaTimeDidChangeToTime:(NSTimeInterval)time {
    NSInteger currentTime = lround(time);
    NSInteger actual = currentTime;
    currentTime += totalSeekDifference;
    if (self.frequency.intValue != 0) {
        if (currentTime % self.frequency.intValue == 0 && currentTime != 0 && lastPlayedSecond != currentTime)  {
            
            //NSLog(@"Play Midroll for: %d - Actual: %d",currentTime,actual);
            lastPlayedSecond = (int)currentTime;
            [self requestMidOrPostRollWithTag:self.midRollTag];
        }
    }
    
    [super videoPlayer:videoPlayer currentMediaTimeDidChangeToTime:time];
}

- (void) requestMidOrPostRollWithTag:(NSString*)tag {
    GMFIMASDKAdService* gmfAdService = (GMFIMASDKAdService*)self.adService;
    [gmfAdService reset];
    gmfAdService.requestingMidOrPostRoll = YES;
    [gmfAdService requestAdsWithRequest:tag companionSlots:[_parentVideoPlayerViewController companionSlots]];
}

- (void) setParentVideoViewController:(UIViewController*)viewController {
    self.parentVideoPlayerViewController = viewController;
}

@end