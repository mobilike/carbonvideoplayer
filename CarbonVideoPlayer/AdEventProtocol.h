//
//  AdEventProtocol.h
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 20/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleInteractiveMediaAds/GoogleInteractiveMediaAds.h>

@protocol AdEventProtocol <NSObject>

- (void)didReceiveAdEvent:(nonnull IMAAdEvent*)event;

@end
