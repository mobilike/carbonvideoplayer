//
//  CBVideoPlayerViewController.h
//  Carbon
//
//  Created by Tolga Caner on 13/04/16.
//  Copyright © 2016 mobilike. All rights reserved.
//

#import "GoogleMediaFramework.h"

@interface CBVideoPlayerViewController : GMFPlayerViewController

- (void) dismiss;
- (void) adClicked;

- (void) setMidRollTag:(NSString*)midRollTag midRollFrequency:(NSNumber*)frequency withPostrollTag:(NSString*)postRollTag;

@property (nonatomic,assign) BOOL dismissUponPostrollFinish;

- (void) setParentVideoViewController:(UIViewController*)viewController;

@end
