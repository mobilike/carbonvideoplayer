//
//  CBVideoStrategy.h
//  Pods
//
//  Created by Tolga Caner on 10/05/16.
//
//

#import <Foundation/Foundation.h>

@protocol CBVideoStrategy <NSObject>

@required
- (void) configure;

@end
