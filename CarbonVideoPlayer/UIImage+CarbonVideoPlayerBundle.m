//
//  UIImage+CarbonVideoPlayerBundle.m
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 06/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import "UIImage+CarbonVideoPlayerBundle.h"

@implementation UIImage (CarbonVideoPlayerBundle)

+ (UIImage *)imageNamed:(NSString *)name inCarbonVideoPlayerBundle:(NSBundle *)bundle
{
    UIImage *imageFromMainBundle = [UIImage imageNamed:name];
    if(imageFromMainBundle)
    {
        return imageFromMainBundle;
    }
    
    NSString *imagePath = [bundle pathForResource:name ofType:@"png"];
    if (!imagePath) {
        imagePath = [bundle pathForResource:[name stringByAppendingString:@"@2x"] ofType:@"png"];
    }
    return [UIImage imageWithContentsOfFile:imagePath];
}

@end
