//
//  CBVideoPremiumStrategy.m
//  Pods
//
//  Created by Tolga Caner on 10/05/16.
//
//

#import "CBVideoPremiumStrategy.h"
#import <WebKit/WKWebView.h>

@implementation CBVideoPremiumStrategy

- (instancetype) initWithVideoPlayerViewController:(CBVideoPlayerViewController*)viewController {
    self = [super init];
    if (self) {
        self.viewController = viewController;
    }
    return self;
}


- (void)configure {
    @try {
        WKWebView *webView;
        self.viewController.playerView.autoresizingMask = UIViewAutoresizingNone;
        [self.viewController.playerView setAutoresizesSubviews:NO];
        for (UIView* aView in [self.viewController.playerView subviews]) {
            if ([aView isKindOfClass:[UIView class]]) {
                if (aView.subviews.count > 0) {
                    for (UIView* bView  in aView.subviews[0].subviews) {
                        if (bView.subviews.count > 0) {
                            if ([bView.subviews[0] isKindOfClass:[UIScrollView class]]) {
                                webView = bView.subviews[1];
                            } else if ([bView.subviews[1] isKindOfClass:[UIScrollView class]]) {
                                webView = bView.subviews[0];
                            }
                        }
                    }
                }
            }
        }
        NSString* jsCommand = @"document.getElementsByClassName(\"videoAdUiTopBar\")[0].style.display='none';" ;
        [webView.superview sendSubviewToBack:webView];
        if ([webView respondsToSelector:@selector(evaluateJavaScript:completionHandler:)]) {
            [webView evaluateJavaScript:jsCommand completionHandler:nil];
        } else if ([webView respondsToSelector:@selector(stringByEvaluatingJavaScriptFromString:)]) {
            if ([webView isKindOfClass:[UIWebView class]]) {
                UIWebView* uiWebView = (UIWebView*) webView;
                [uiWebView stringByEvaluatingJavaScriptFromString:jsCommand];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
    }
    //NSLog(@"Configuring for video premium strategy");
}

@end
