//
//  CBVideoStrategyContext.m
//  Pods
//
//  Created by Tolga Caner on 10/05/16.
//
//

#import "CBVideoStrategyContext.h"

@implementation CBVideoStrategyContext

- (void) configure {
    [_strategy configure];
}

@end
