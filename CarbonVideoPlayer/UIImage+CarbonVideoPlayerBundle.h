//
//  UIImage+CarbonVideoPlayerBundle.h
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 06/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CarbonVideoPlayerBundle)

+ (UIImage *)imageNamed:(NSString *)name inCarbonVideoPlayerBundle:(NSBundle *)bundle;

@end
