//
//  NSString+SHAOne.h
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 05/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SHAOne)

- (NSString *)sha1FromString;

@end
